package rpsgame;
import java.util.Scanner;

import backend.RpsGame;

 /**
 * @description Console application for rock paper scissors game
 * @author Desiree Mariano 2236344
 * @version 9/12/2023
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner scan = new Scanner(System.in);
        RpsGame game = new RpsGame();
        
        while (true){
            System.out.println(" ");
            System.out.println("What would you like to do? ");
            System.out.println("(1) Play a game");
            System.out.println("(2) Exit");
            System.out.print("Enter your choice here: ");
            int choice = scan.nextInt();
            scan.nextLine();

            switch(choice){
                case 1:
                    System.out.println(" ");
                    System.out.print("Enter rock, paper or scissors: ");
                    String userChoice = scan.nextLine();
                    String message = game.playRound(userChoice);
                    System.out.println(message);
                    System.out.print(" / Wins: " + game.getWins());
                    System.out.print(" / Losses:  " +  game.getLosses());
                    System.out.print(" / Ties: " +  game.getTies());
                    break;
                case 2:
                    // Exit
                    System.out.println("Exit complete! Goodbye!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice");
                break;
            }
        }
    }
}
