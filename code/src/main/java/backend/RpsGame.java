package backend;
import java.util.Random;


/**
 * @description RpsGame handles the overall game and how it is played
 * @author Desiree Mariano 2236344
 * @version 9/12/2023
 */

public class RpsGame {
    private int wins = 0;
    private int ties = 0;
    private int losses = 0;
    Random rand = new Random();

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public String playRound(String userChoice) {
        int computerNum = rand.nextInt(3);
        String computerChoice;

        // Assigning computerNum to be a String for the game
        if (computerNum == 0) {
            computerChoice = "rock";
        } else if (computerNum == 1) {
            computerChoice = "scissors";
        } else {
            computerChoice = "paper";
        }

        // Checking ties
        if (userChoice.equals(computerChoice)) {
            ties++;
            return "It's a tie! Both chose " + userChoice;
        }

        // Checking User wins
        else if ((userChoice.equals("rock") && computerChoice.equals("scissors")) ||
                (userChoice.equals("scissors") && computerChoice.equals("paper")) ||
                (userChoice.equals("paper") && computerChoice.equals("rock"))) {
        wins++;
        return "User wins! " + userChoice + " beats " + computerChoice;
        }

        // Checking User losses
        else {
            losses++;
            return "Computer wins! " + computerChoice + " beats " + userChoice;
        }
    }
}