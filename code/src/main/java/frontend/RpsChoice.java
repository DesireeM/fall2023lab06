/**
 * @description RpsChoice class handles ui for the RpsApplication class once running
 * @author Desiree Mariano 2236344
 * @version 9/12/2023
 */

package frontend;

import backend.RpsGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent> {

    //fields
    private String playerChoice;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private TextField message;
    private RpsGame game;

    public RpsChoice(String playerChoice, TextField wins, TextField losses, TextField ties, TextField message, RpsGame game){
        this.playerChoice = playerChoice;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.message = message;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e){
       String messageUI = game.playRound(playerChoice);
       int winCount = game.getWins();
       int lossCount = game.getLosses();
       int tieCount = game.getTies();

       wins.setText("Wins: " +winCount);
       losses.setText("Losses: " +lossCount);
       ties.setText("Ties: " +tieCount);

       message.setText(messageUI);
    }
    
}
